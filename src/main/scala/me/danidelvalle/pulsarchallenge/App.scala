package me.danidelvalle.pulsarchallenge

import com.spotify.scio.ContextAndArgs
import me.danidelvalle.pulsarchallenge.job.{DictionaryJob, InverseIndexJob}

/**
  * Challenge main app
  */
object App extends Logger {

  /**
    * Program entry point
    * @param cmdlineArgs command line args
    */
  def main(cmdlineArgs: Array[String]): Unit = {

    logger.info("Starting pulsar challenge app")
    logger.info("Parsing the command line arguments and creating the scio context")
    implicit val (sc, args) = ContextAndArgs(cmdlineArgs)

    // define the scio pipeline to build the dictionary and the inverse index
    val dictionary = DictionaryJob.build
    InverseIndexJob.build(dictionary)

    // block until the pipeline is finished
    logger.info("Run the pipeline to create the dictionary and the inverse index")
    sc.close().waitUntilFinish()
  }
}
