package me.danidelvalle

import com.spotify.scio.values.SCollection

import scala.collection.SortedSet
import scala.util.hashing.MurmurHash3.stringHash

/**
  * package types definitions
  */
package object pulsarchallenge {

  /**
    * WordId type
    */
  type WordId = Long

  /**
    * A very simple way to generate an positive ID for each word.
    * @param word word
    * @return the long positive id
    */
  def getWordId(word: String): WordId = stringHash(word).toLong + Int.MaxValue.toLong

  /**
    * Case class to model a word
    * @param value word value
    * @param id word id
    */
  case class Word(value: String, id: Long) {

    /**
      * Get the (word,id) pair
      * @return (word,id) pair
      */
    def asPair(): (String, Long) = (value, id)
  }

  /**
    * Word companion object to provide a constructor that
    * just needs the word value.
    */
  object Word {
    /**
      * Create a word instance with just the value
      * @param word the value
      * @return a word instance
      */
    def apply(word: String): Word = Word(word, getWordId(word))
  }

  /**
    * DocumentId type
    */
  type DocumentId = Long

  /**
    * The Dictionary type using scio SCollection
    */
  type Dictionary = SCollection[(String, WordId)]

  /**
    * A type for a SCollection of (DocumentId, Word) pairs
    */
  type DocumentWordPairCollection = SCollection[(DocumentId, Word)]

  /**
    * A type for a SCollection of (WordId, DocumentId) pairs
    */
  type WordIdDocumentIdPairCollection =  SCollection[(WordId, DocumentId)]

  /**
    * A type for the InverseIndex using a SCollection
    */
  type InverseIndex = SCollection[(WordId, SortedSet[DocumentId])]

}
