package me.danidelvalle.pulsarchallenge.job

import com.spotify.scio.{Args, ScioContext}
import me.danidelvalle.pulsarchallenge.{Dictionary, DocumentWordPairCollection}

/**
  * Job to build the dictionary
  */
object DictionaryJob extends Reader {

  /**
    * Build the dictionary
    * @param args program args
    * @param sc scio context
    * @return the dictionary
    */
  def build(implicit args: Args, sc: ScioContext): Dictionary = {

    // read the collection of pairs (document, normalized word)
    val docToWords: DocumentWordPairCollection = read(args("dictionaryInput"))

    val dictionary = docToWords.values.distinct.map(_.asPair)

    dictionary.map(_.productIterator.mkString(" ")).saveAsTextFile(args("dictionaryOutput"), 1)

    dictionary
  }
}
