package me.danidelvalle.pulsarchallenge.job

import com.spotify.scio._
import me.danidelvalle.pulsarchallenge.{Dictionary, DocumentId, InverseIndex}

import scala.collection.SortedSet

/**
  * Job to get the InverseIndex.
  */
object InverseIndexJob extends Reader {

  /**
    * Run the job.
    * @param dictionary dictionary
    * @param args program args
    * @param sc scio context
    * @return the inverse index
    */
  def build(dictionary: Dictionary)
           (implicit args: Args, sc: ScioContext): InverseIndex = {

    // get the word,documentid pair collection
    val wordDocumentIdsPairCollection = read(dictionary, args("indexInput"))

    /*
     Get the index by aggregating by key (the word) and storing all documentIds in a sorted set
     */
    val index = wordDocumentIdsPairCollection.aggregateByKey(SortedSet[DocumentId]())(_ + _, _ ++ _)

    // save the index
    index
      .map { case (word, dictionaryIdSet) => s"$word: [${dictionaryIdSet.mkString(",")}]" }
      .saveAsTextFile(args("indexOutput"), 1)

    index
  }

}
