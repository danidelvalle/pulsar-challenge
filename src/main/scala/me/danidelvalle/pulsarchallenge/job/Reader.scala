package me.danidelvalle.pulsarchallenge.job

import com.spotify.scio.values.SCollection
import com.spotify.scio.{Args, ScioContext}
import me.danidelvalle.pulsarchallenge._
import org.apache.beam.sdk.io.FileSystems

import scala.collection.JavaConverters._

/**
  * Reader trait to provide the methods to read the dataset as
  * SCollections.
  */
trait Reader {


  /**
    * Read the input dataset and return a DocumentWordPairCollection. This
    * read method is intented to be used in the DictionaryJob.
    * @param input input to read
    * @param sc scio context
    * @return the DocumentWordPairCollection
    */
  def read(input: String)(implicit sc: ScioContext): DocumentWordPairCollection  = {
    sc.unionAll(
      getDocumentsToRead(input).map(doc => sc.textFile(doc)
        .flatMap(_
          .split("\\W+")
          .filter(_.nonEmpty)
          .map(w => Word(w.toLowerCase)))
        .distinct
        .keyBy(_ => doc.substring(doc.lastIndexOf("/") + 1).toLong)
      )
    )
  }

  /**
    * Read the in put dataset and, using the dictionary, return the
    * WordIdDocumentIdPairCollection. This read method is designed to be
    * used from the InverseIndex job.
    * @param dictionary the dictionary
    * @param input input to read
    * @param sc scio context
    * @return the WordIdDocumentIdPairCollection
    */
  def read(dictionary: Dictionary, input: String)(implicit sc: ScioContext):
    WordIdDocumentIdPairCollection = {
    sc.unionAll(
      getDocumentsToRead(input).map(doc => {
        val docId = doc.substring(doc.lastIndexOf("/") + 1).toLong
        sc.textFile(doc)
          .flatMap(_
            .split("\\W+")
            .filter(_.nonEmpty)
            .map(_.toLowerCase)
          )
          .distinct
          .map(w => (w, docId))
          .join(dictionary)
          .map { case (w, (docId, wordId)) => (wordId, docId) }
      })
    )

  }

  /**
    * Given the args get the matched source files to read
    * @param input input to read
    * @return a list of uris to read
    */
  def getDocumentsToRead(input: String): List[String] = {
    if (input.contains("*")) {
      FileSystems.`match`(input).metadata().asScala.map(_.resourceId().toString).toList
    } else {
      List(input)
    }
  }

}
