package me.danidelvalle.pulsarchallenge

import org.slf4j.LoggerFactory

/**
  * Logger trait
  */
trait Logger {

  /**
    * Logger
    */
  private[pulsarchallenge] val logger = LoggerFactory.getLogger(this.getClass)
}
