package me.danidelvalle.pulsarchallenge

import com.spotify.scio.io.TextIO
import com.spotify.scio.testing._

/**
  * Pulsar challenge tests
  */
class AppSpec extends PipelineSpec {

  /**
    * Input filename
    */
  val inputFileName = "10"

  /**
    * Input test data
    */
  val inData = Seq("a b c d e", "a b a b")

  /**
    * Unique test words
    */
  val words = Seq("a", "b", "c", "d", "e")

  /**
    * Test word ids
    */
  val wordIds = words.map(getWordId)

  /**
    * Expected dictionary
    */
  val expectedDictionary = words.zip(wordIds).map(_.productIterator.mkString(" "))


  /**
    * Expected index
    */
  lazy val expectedIndex = wordIds.map(id => s"$id: [$inputFileName]")


  /**
    * Test the app to check it builds both the index and the dictionary.
    * Two input file names are required to avoid an exception:
    * `requirement failed: Test input TextIO(input) has already been read from once.`
    * */
  "PulsarChallengeApp" should "create the dictionary and the index" in {
    JobTest[App.type]
      .args("--dictionaryInput=11", "--dictionaryOutput=dictionary.txt",
        "--indexOutput=index.txt", s"--indexInput=$inputFileName")
      .input(TextIO(inputFileName), inData)
      .input(TextIO("11"), inData)
      .output(TextIO("dictionary.txt"))(_ should containInAnyOrder (expectedDictionary))
      .output(TextIO("index.txt"))(_ should containInAnyOrder (expectedIndex))
      .run()
  }

}
