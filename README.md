# House of Pulsar Challenge

## Exercise 1

As the "DNA Coding Challenge.pdf" indicates, I wrote a solution using Scio shown in this repository. The stack includes:

* **scio 0.7.4, beam 2.11.0 with scala 2.12.8**
* Project Management: **sbt**
* Style checker: **Scalastyle**
* Logging: **slf4j-simple**
* Testing: **scio-test** (based on **scalatest**)
* CI: **Jenkins** (just a stub as reference)

Some considerations:

* This is my first experience with scio and beam. I had read a bit about beam, but never had the opportunity to make a PoC or use it in a project. I could have done the challenge with spark/scala, but I guess that the objective is to show how I would face a new problem totally out of my comfort zone.
* I setup the project using the [gitter8 template](https://github.com/spotify/scio.g8) based on scio 0.7.4, which is the last stable release so far.
* I run both tasks (build the dictionary and the index) in the same pipeline. I considered to have two pipelines (one for the dictionary and for the index) and use the pipeline orchestion as in [WordCountOrchestration.scala](https://spotify.github.io/scio/examples/WordCountOrchestration.scala.html), however the example use features to be included in next releases (like getting a future tap from the ScioResult). I usually prefer to avoid snapshot versions.
* I could not meet one of the requirements: to sort the index by the word ids. I do invest some time in understand how scio words and try to figure out a "sort by key" feature. I have not implemented it to do not delay my submission, but I would solve this by saving to a sink that would allow me to read again in a sorted manner, like BigQuery.

## Exercise 2

### REST API Design

Please find [here](https://pulsarchallenge.docs.apiary.io/) a brief API documentation. Only read methods are provided.

### Build

I would implement the API using the [play framework](https://www.playframework.com/) because is level of maturity and scalability. Of course the project should follow the  style guidelines listed before (scalastyle, jenkins or other CI/CD tool, logging and testing).

### Deploy (AWS)

I'd use the following aws products to deploy and monitor the API:

* I'd build a stage to jenkins to build a docker image and publish it to the aws registry (ECR).
* I'd build a ECS cluster with an autoscaling policy to automatically adapt the resources to the API load.
* The API containers could just print the log messages to stdout so aws will take care of them through CloudWatch, where metrics and alarms could be defined to monitor the service.
* Implement an OAuth 2.0 authentication with Cognito.
* Finally, use CloudFormation to define all described infrastructure in a versioned text template (json or yaml).
